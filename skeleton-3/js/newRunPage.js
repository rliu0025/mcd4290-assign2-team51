// Code for the Measure Run page.
//Display the map centered at Monash Clyton 
mapboxgl.accessToken = 'pk.eyJ1Ijoic2hpdmFtMDA5IiwiYSI6ImNqem02OTI0OTBobnQzbW15d3IwMW80MWMifQ.-uwI8vlQTLnzE3Lb9_PJYw';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 16,
    
});
        
//Creat a current location marker
var locationMarker = new mapboxgl.Marker({
    color: "red"
});
//Set current location maker popup
var locationMarkerPopup = new mapboxgl.Popup({
    offset: 45
});
locationMarker.setPopup(locationMarkerPopup);
        
//Get your current location and put the marker
navigator.geolocation.getCurrentPosition(function(position) {
    var yourLocation = [position.coords.longitude, position.coords.latitude];
    //Pan to your current location
    map.panTo(yourLocation);  
    //Put the marker at your current location
    locationMarker.setLngLat(yourLocation);
    locationMarker.addTo(map); 
    //Account for location inaccuracy
    var inaccuracy = position.coords.accuracy
    var text = 'You are here. Inaccuracy: ' + inaccuracy + ' meters';
    locationMarkerPopup.setText(text);
});
   

        function showPolygon()
        {
            // Code added here will run when the "Show Polygon" button is clicked.
        }

        // This function checks whether there is a map layer with id matching 
        // idToRemove.  If there is, it is removed.
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        }
        
 function randomDestination () {
            navigator.geolocation.getCurrentPosition(position => {
                console.log(position)
                var currentLatitude, currentLongitude, runLongitude, runLatitude
                currentLatitude = position.coords.latitude;
                currentLongitude = position.coords.longitude;
                runLatitude = (currentLatitude + (Math.random() / 1000));
                runLongitude = (currentLongitude + (Math.random() / 1000));
                console.log(runLatitude)
                let marker;
                marker = new mapboxgl.Marker({
                    color: "green"
                });
                marker.setLngLat([runLongitude, runLatitude]);
                marker.addTo(map);
                map.addLayer({
                    "id": "route",
                    "type": "line",
                    "source": {
                        "type": "geojson",
                        "data": {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "LineString",
                                "coordinates": [
                                    [currentLongitude, currentLatitude],
                                    [runLongitude, runLatitude]
                                ]
                            }
                        }
                    },
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 8
                    }
                });
            });
        };
        
