// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];




function getCurrentTime() {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;

    return dateTime;
}

class Run {
    constructor(longitude, latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.startDateTime = getCurrentTime();
    }

    saveToLocalStore(run) {
        localStorage.setItem(
            this.startDateTime, JSON.stringify(
                {
                    longitude: this.longitude,
                    latitude: this.latitude,
                    startDateTime: this.startDateTime
                })
        );
    }

    static getAllFromLocalStore() {
        var archive = [],
            keys = Object.keys(localStorage),
            i = 0,
            key;

        for (; key = keys[i]; i++) {
            var temp = { [key]: JSON.parse(localStorage.getItem(key)) };

            archive.push(temp);
        }

console.log(archive);

        return archive;
    }
}