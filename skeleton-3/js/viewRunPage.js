// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    var runNames = [ "Run A", "Run B" ];
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}
// Code for the Record Region page.

/*
//Display the map centered at Monash Clyton 
mapboxgl.accessToken = 'pk.eyJ1IjoibWNkNDI5MHRlYW02MiIsImEiOiJjanY2bTlubzEwNnFlNGRvNTZ0YTJ0anduIn0._oNsmNwu0HnMN6yL3-DD4Q';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 16,
    center: [145.1343136,-37.9110467]
});


//Creat a current location marker
var locationMarker = new mapboxgl.Marker({
    color: "red"
});
//Set current location maker popup
var locationMarkerPopup = new mapboxgl.Popup({
    offset: 45
});
locationMarker.setPopup(locationMarkerPopup);

//Get your current location and put the marker
navigator.geolocation.getCurrentPosition(function(position) {
    var yourLocation = [position.coords.longitude, position.coords.latitude];
    //Pan to your current location
    map.panTo(yourLocation);  
    //Put the marker at your current location
    locationMarker.setLngLat(yourLocation);
    locationMarker.addTo(map); 
    //Account for location inaccuracy
    var inaccuracy = position.coords.accuracy
    var text = 'You are here. Inaccuracy: ' + inaccuracy + ' meters';
    locationMarkerPopup.setText(text);
});

//Watch your current location 
var yourLocation, inaccuracy;
var watchID = navigator.geolocation.watchPosition(function(position) {
    yourLocation = [position.coords.longitude, position.coords.latitude];
    map.panTo(yourLocation); 
    //Reput the pin at your current location
    locationMarker.setLngLat(yourLocation);  
    inaccuracy = position.coords.accuracy
    var text = 'You are here. Inaccuracy: ' + inaccuracy + ' meters';
    locationMarkerPopup.setText(text);
    //console.log(yourLocation);
});
*/